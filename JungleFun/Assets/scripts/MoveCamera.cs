using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCamera : MonoBehaviour
{
    public Transform positionHero;
    public int forceMove;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(positionHero.position.x, positionHero.position.y, positionHero.position.z - 10);
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 positionPlayer = new Vector3(positionHero.position.x, positionHero.position.y, positionHero.position.z - 10);
        Vector3 pos = Vector3.Lerp(transform.position, positionPlayer, forceMove *Time.deltaTime);
        transform.position = pos;
    }
}
