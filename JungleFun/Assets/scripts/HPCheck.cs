using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HPCheck : MonoBehaviour
{
    public Text TextHeroHP;
    public Text TextEnemyHP;
    public Text KillsOneGame;
    public static int KillsInGame;

    void Update()
    {

        TextHeroHP.text = HeroProperty.HP.ToString();

        KillsOneGame.text = KillsInGame.ToString();
    }
}
    //void OnCollisionEnter2D(Collision2D col)
    //{
    //    TextEnemyHP.text = col.EnemyHP.ToString();

    //}
