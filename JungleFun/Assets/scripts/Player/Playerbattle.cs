using UnityEngine;

public class Playerbattle : MonoBehaviour
{
    public GameObject SwordObj;
    public GameObject RealSword;

    public int PowerPush = 10;
    float hypotenuse;


    float xPosition;
    float yPosition;
    float cosXY;
    float sinXY;

    

    private void OnTriggerStay2D(Collider2D col)
    {

        if ("Enemy" == col.gameObject.tag)
        {
            GameObject enemy = col.gameObject;
            Rigidbody2D EnemyRb = enemy.GetComponent<Rigidbody2D>();
            Transform EnemyTF = enemy.GetComponent<Transform>();
            EnemyProperty EnemyHP = enemy.GetComponent<EnemyProperty>();
            xPosition = EnemyTF.position.x - transform.position.x;
            yPosition = EnemyTF.transform.position.y - transform.position.y;
            hypotenuse = Mathf.Sqrt(Mathf.Pow(xPosition, 2) + Mathf.Pow(yPosition, 2));
            cosXY = yPosition / hypotenuse;
            sinXY = xPosition / hypotenuse;

            SwordObj.transform.rotation = Quaternion.Euler(0, 0, Mathf.Atan2((EnemyTF.position.y - transform.position.y), (EnemyTF.position.x - transform.position.x)) * Mathf.Rad2Deg);

            if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.F))
            {
               
                RealSword.SetActive(true);
                Invoke("DeActivateSword", 0.2f);
                EnemyRb.velocity = (PowerPush * new Vector2(sinXY, cosXY));
                if (EnemyHP.hp > 0)
                    EnemyHP.hp -= 20;
                else
                {
                    EnemyHP.hp = 100;
                    HeroProperty.HP += 200;
                }
             
            }
        }
    }


    void DeActivateSword() // �� �������!!!
    {
        RealSword.SetActive(false);
    }
}
