using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextEnemy : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
        EnemyProperty enemyProperty = GetComponentInParent<EnemyProperty>();
        Text text = gameObject.GetComponent<Text>();
        text.text = enemyProperty.hp.ToString();
    }
}
