using UnityEngine;

public class EnemyMoveV2 : MonoBehaviour
{
    
    public Transform heroTF;
    public Rigidbody2D HeroRb;
    float xPosition;
    float yPosition;
    float cosXY;
    float sinXY;
    public float PowerPush = 15;
    private void Start()
    {
        if (heroTF == null || HeroRb == null)
        {
            heroTF = GameObject.FindGameObjectWithTag("hero").transform;
            HeroRb = GameObject.FindGameObjectWithTag("hero").GetComponent<Rigidbody2D>();
        }
    }
    void Update()
    {
        MoveToHero();
    }

    void MoveToHero()
    {
        xPosition = heroTF.position.x - transform.position.x;
        yPosition = heroTF.transform.position.y - transform.position.y;
        cosXY = yPosition / Mathf.Sqrt(Mathf.Pow(xPosition, 2) + Mathf.Pow(yPosition, 2));
        sinXY = xPosition / Mathf.Sqrt(Mathf.Pow(xPosition, 2) + Mathf.Pow(yPosition, 2));
        transform.position += Time.deltaTime * 2f * (new Vector3(sinXY, cosXY));

    }

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "hero")
        {
            if (HeroProperty.HP > 0)
            {
                HeroProperty.HP -= 1;
                HeroRb.velocity = PowerPush * new Vector3(sinXY, cosXY);

            }
            else
            {
                HPCheck.KillsInGame = 0;
                HeroProperty.HP = 100;
            }
        }
    }
}
